::===============================================================================================================
:: CHANNEL: CURRENT (MONTHLY)
::===============================================================================================================
	@echo off
	cls
	setLocal EnableExtensions EnableDelayedExpansion
	set "OfficeRToolpath=%~dp0"
	set "OfficeRToolpath=%OfficeRToolpath:~0,-1%"
	set "OfficeRToolname=%~n0.cmd"

	set "pswindowtitle=$Host.UI.RawUI.WindowTitle = 'Administrator: OfficeRTool - 2020/June/12 -'"
	cd /D "%OfficeRToolpath%"
::===============================================================================================================
:: CHECK ADMIN RIGHTS
::===============================================================================================================
	fltmc >nul 2>&1
	if "%errorlevel%" NEQ "0" (goto:UACPrompt) else (goto:GotAdmin)
::===============================================================================================================
::===============================================================================================================
:UACPrompt
	echo:
	echo   Requesting Administrative Privileges...
	echo   Press YES in UAC Prompt to Continue
	echo:
	echo Set UAC = CreateObject^("Shell.Application"^) > "%temp%\GetAdmin.vbs"
	echo args = "ELEV " >> "%temp%\GetAdmin.vbs"
	echo For Each strArg in WScript.Arguments >> "%temp%\GetAdmin.vbs"
	echo args = args ^& UCase(strArg) ^& " "  >> "%temp%\GetAdmin.vbs"
	echo Next >> "%temp%\GetAdmin.vbs"
    echo UAC.ShellExecute "%OfficeRToolname%", args, "%OfficeRToolpath%", "runas", 1 >> "%temp%\GetAdmin.vbs"
    cmd /u /c type "%temp%\GetAdmin.vbs">"%temp%\GetAdminUnicode.vbs"
    cscript //nologo "%temp%\GetAdminUnicode.vbs" %1
    del /f /q "%temp%\GetAdmin.vbs" >nul 2>&1
    del /f /q "%temp%\GetAdminUnicode.vbs" >nul 2>&1
    exit /B
::===============================================================================================================
::===============================================================================================================
:GotAdmin
	if /I '%1'=='ELEV' shift /1
    if exist "%temp%\GetAdmin.vbs" del /f /q "%temp%\GetAdmin.vbs"
	if exist "%temp%\GetAdminUnicode.vbs" del /f /q "%temp%\GetAdminUnicode.vbs"
::===============================================================================================================
::===============================================================================================================
	cls
	mode con cols=82 lines=48
	color 1F
	echo:
::===============================================================================================================
:: DEFINE SYSTEM ENVIRONMENT
::===============================================================================================================
	for /F "tokens=6 delims=[]. " %%A in ('ver') do set /a win=%%A
	if %win% LSS 7601 (echo:)&&(echo:)&&(echo Unsupported Windows detected)&&(echo:)&&(echo Minimum OS must be Windows 7 SP1 or better)&&(echo:)&&(goto:TheEndIsNear)
	for /F "tokens=2 delims==" %%a in ('wmic path Win32_Processor get AddressWidth /value') do (set winx=win_x%%a)
	set "sls=SoftwareLicensingService"
	set "slp=SoftwareLicensingProduct"
	set "osps=OfficeSoftwareProtectionService"
	set "ospp=OfficeSoftwareProtectionProduct"
	for /F "tokens=2 delims==" %%A in ('"wmic path %sls% get version /VALUE" 2^>nul') do set "slsversion=%%A"
	if %win% LSS 9200 (for /F "tokens=2 delims==" %%A IN ('"wmic path %osps% get version /VALUE" 2^>nul') do set ospsversion=%%A)
	set "downpath=not set"
	set "o16updlocid=not set"
	set "o16arch=x86"
    set "o16lang=en-US"
	set "langtext=Default Language"
    set "o16lcid=1033"
::===============================================================================================================
:: Read OfficeRTool.ini
::===============================================================================================================
	SET /a countx=0
	cd /D "%OfficeRToolpath%"
	for /F "tokens=*" %%a in (OfficeRTool.ini) do (
		SET /a countx=!countx! + 1
		set var!countx!=%%a
	)
	if %countx% LSS 10 (echo:)&&(echo Error in OfficeRTool.ini)&&(echo:)&&(pause)&&(goto:Office16VnextInstall)
	set "inidownpath=%var3%"
	if "%inidownpath:~-1%" EQU " " set "inidownpath=%inidownpath:~0,-1%"
	set "downpath=%inidownpath%"
	set "inidownlang=%var6%"
	if "%inidownlang:~-1%" EQU " " set "inidownlang=%inidownlang:~0,-1%"
	set "o16lang=%inidownlang%"
	set "inidownarch=%var9%"
	if "%inidownarch:~-1%" EQU " " set "inidownarch=%inidownarch:~0,-1%"
	set "o16arch=%inidownarch%"
::===============================================================================================================
::===============================================================================================================
:: Check Microsoft Content Delivery Network (CDN) server for new Office releases
::===============================================================================================================
	echo:
	echo Checking public Office distribution channels for new updates
	call :CheckNewVersion Current 492350f6-3a01-4f97-b9c0-c7c6ddf67d60
::	call :CheckNewVersion CurrentPreview 64256afe-f5d9-4f86-8936-8840a6a4f5be
::	call :CheckNewVersion BetaChannel 5440fd1f-7ecb-4221-8110-145efaa6372f
::	call :CheckNewVersion MonthlyEnterprise 55336b82-a18d-4dd6-b5f6-9e5095c314a6 
::	call :CheckNewVersion SemiAnnual 7ffbc6bf-bc32-4f92-8982-f9dd17fd3114
::	call :CheckNewVersion SemiAnnualPreview b8f9b850-328d-4355-9145-c59439a0c4cf
::	call :CheckNewVersion DogfoodDevMain ea4a4090-de26-49d7-93c1-91bff9e53fc3
	((echo:)&&(echo:)&&(echo:)&&(goto :updateconfig))
::===============================================================================================================
::===============================================================================================================	
:CheckNewVersion
	set "o16build=not set "
	set "o16latestbuild=not set"
	if "%1" EQU "Manual_Override" goto:CheckNewVersionSkip1
	if exist "%OfficeRToolpath%\latest_%1_build.txt" (
		set /p "o16build=" <"%OfficeRToolpath%\latest_%1_build.txt"
		)
	set "o16build=%o16build:~0,-1%"
:CheckNewVersionSkip1
	echo:
	if %win% GEQ 9200 "%OfficeRToolpath%\OfficeFixes\%winx%\wget.exe" --no-verbose --output-document="%TEMP%\VersionDescriptor.txt" --tries=20 "https://mrodevicemgr.officeapps.live.com/mrodevicemgrsvc/api/v2/C2RReleaseData/?audienceFFN=%2" >nul 2>&1
	if %win% LSS 9200 "%OfficeRToolpath%\OfficeFixes\%winx%\wget.exe" --no-verbose --output-document="%TEMP%\VersionDescriptor.txt" --tries=20 "https://mrodevicemgr.officeapps.live.com/mrodevicemgrsvc/api/v2/C2RReleaseData/?audienceFFN=%2&osver=Client|6.1.0" >nul 2>&1
	if %errorlevel% GEQ 1 goto:ErrCheckNewVersion1
	type "%TEMP%\VersionDescriptor.txt" | find "AvailableBuild" >"%TEMP%\found_office_build.txt"
	if %errorlevel% GEQ 1 goto:ErrCheckNewVersion2
	set /p "o16latestbuild=" <"%TEMP%\found_office_build.txt"
	set "o16latestbuild=%o16latestbuild:~21,16%"
	set "spaces=     "
	if "%o16latestbuild:~15,1%" EQU " " ((set "o16latestbuild=%o16latestbuild:~0,14%")&&(set "spaces=       "))
	if "%o16latestbuild:~0,3%" NEQ "16." goto:ErrCheckNewVersion3
	(echo --^> Checking channel:      %1)
	if "%1" EQU "Manual_Override" goto:CheckNewVersionSkip2a
	if "%o16build%" NEQ "%o16latestbuild%" (
		powershell -noprofile -command "%pswindowtitle%"; Write-Host "'   'New Build available:'   '" -foreground "White" -nonewline; Write-Host "%o16latestbuild%'%spaces%'" -foreground "Red" -nonewline; Write-Host "LkgBuild:' '" -foreground "White" -nonewline; Write-Host "%o16build%" -foreground "Green"
		echo %o16latestbuild% >"%OfficeRToolpath%\latest_%1_build.txt"
		echo %o16build% >>"%OfficeRToolpath%\latest_%1_build.txt"
		goto:updateconfig
		)
:CheckNewVersionSkip2a
	powershell -noprofile -command "%pswindowtitle%"; Write-Host "'   'Last known good Build:' '" -foreground "White" -nonewline; Write-Host "%o16latestbuild%'%spaces%'" -foreground "Green" -nonewline; Write-Host "No newer Build available" -foreground "White"
:CheckNewVersionSkip2b
	del /f /q "%TEMP%\VersionDescriptor.txt"
	del /f /q "%TEMP%\found_office_build.txt"
	set "buildcheck=ok"
	goto:eof
:ErrCheckNewVersion1
	echo:
	powershell -noprofile -command "%pswindowtitle%"; Write-Host "*** ERROR checking: * %1 * channel" -foreground "Red"
	powershell -noprofile -command "%pswindowtitle%"; Write-Host "*** No response from Office content delivery server" -foreground "Red"
	echo:
	echo Check Internet connection and/or Channel-ID.
	set "buildcheck=not ok"
	goto:eof
:ErrCheckNewVersion2
	echo:
	powershell -noprofile -command "%pswindowtitle%"; Write-Host "*** ERROR checking: * %1 * " -foreground "Red"
	powershell -noprofile -command "%pswindowtitle%"; Write-Host "*** No Build / Version number found in file: * VersionDescriptor.txt" -foreground "Red"
	copy "%TEMP%\VersionDescriptor.txt" "%TEMP%\%1_VersionDescriptor.txt" >nul 2>&1
	echo:
	echo Check file "%TEMP%\%1_VersionDescriptor.txt"
	set "buildcheck=not ok"
	goto:eof
:ErrCheckNewVersion3
	echo:
	powershell -noprofile -command "%pswindowtitle%"; Write-Host "*** ERROR checking: * %1 * " -foreground "Red"
	powershell -noprofile -command "%pswindowtitle%"; Write-Host "*** Unsupported Build / Version number detected: * %o16latestbuild% *" -foreground "Red"
	copy "%TEMP%\VersionDescriptor.txt" "%TEMP%\%1_VersionDescriptor.txt" >nul 2>&1
	copy "%TEMP%\found_office_build.txt" "%TEMP%\%1_found_office_build.txt" >nul 2>&1
	echo:
	echo Check file "%TEMP%\%1_VersionDescriptor.txt"
	echo Check file "%TEMP%\%1_found_office_build.txt"
	set "buildcheck=not ok"
	goto:eof
::===============================================================================================================
:updateconfig
SETLOCAL EnableExtensions
set file=C2R_Configold_20201021-1618.ini
set newline=Version=%o16latestbuild%
set insertline=4

REM Use Set Output=Con to send to screen without amending code, or set it to a filename to make a new file
set output=C2R_Config1_20201021-1618.ini
(for /f "tokens=1* delims=[]" %%a in ('find /n /v "##" ^< "%file%"') do (
if "%%~a"=="%insertline%" (
echo %newline%
REM ECHO.%%b
) ELSE (
echo.%%b
)
)) > %output%
set file=C2R_Config1_20201021-1618.ini
set newline=[configuration]
set insertline=1

REM Use Set Output=Con to send to screen without amending code, or set it to a filename to make a new file
set output=C2R_Config_20201021-1618.ini
(for /f "tokens=1* delims=[]" %%a in ('find /n /v "##" ^< "%file%"') do (
if "%%~a"=="%insertline%" (
echo %newline%
REM ECHO.%%b
) ELSE (
echo.%%b
)
)) > %output%
ENDLOCAL
goto:CheckNewVersionSkip2b
