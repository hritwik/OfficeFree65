@echo off
set %1
set %2
set %3
set %4
set %5
::if not "%1"=="am_admin" (powershell start -verb runas '%0' am_admin & exit /b)
mode con:cols=100 lines=20
cls
color fc
if "%debug%" == "1" echo DEBUG promptanyway = %promptanyway% - calledfromscript = %calledfromscript% && pause && goto debugprompt
goto directions

:directions
if "%promptanyway%" == "1" goto prompt
if "%calledfromscript%" == "1" goto cleanup
goto prompt

:prompt
cls
echo.
if "%debug%" == "1" echo Debug mode is ON
if "%calledfromscript1%" == "1" echo Skip to full cleanup setting enabled.
if "%calledfromscript1%" == "1" echo Downloading fullcleanup.bat to %~f0
if "%calledfromscript1%" == "1" echo Launching %~f0
if "%calledfromscript1%" == "1" mode con:cols=120 lines=30
echo  -----------------------------------------------------------------------------------------------
echo   This script is a part of
echo   OfficeFree65 v2.0.0 by 3rik https://codeberg.org/3rik/OfficeFree65/
echo  -----------------------------------------------------------------------------------------------
echo.
echo  -----------------------------------------------------------------------------------------------
echo   Sometimes this script leaves some files behind that it isn't able to clean up itself.
echo.
echo   This can happen if it is accidentally closed before completion, if it runs into an error
echo   before completion or similar.
echo.
echo   Running this script will delete every file and folder created by the script
echo   If some steps fail, you may want to restart your computer and run this script again.
echo  -----------------------------------------------------------------------------------------------
echo.
echo   Press any key to begin process . . .
pause >nul
cls
if "%calledfromscript%" == "1" goto cleanup
goto cleanuptwo

:cleanup
cd %appdata%
if "%debug%" == "1" echo %~f0
if "%debug%" == "1" pause>nul|set/p =DEBUG-Delete %appdata%\officeinstallation next-Press any key to continue...
rmdir /q /s %appdata%\officeinstallation
if "%debug%" == "1" pause>nul|set/p =DEBUG-Delete %appdata%\deleteoldoffice next-Press any key to continue...
rmdir /q /s %appdata%\deleteoldoffice
if "%debug%" == "1" pause>nul|set/p =DEBUG-Delete %appdata%\3rikscript.bat next-Press any key to continue...
del %appdata%\3rikscript.bat
if "%debug%" == "1" pause>nul|set/p =DEBUG - scriptlocation = %scriptlocation% -run file at scriptlocation variable-Press any key to continue...
start %scriptlocation% "CALLEDFROMCLEANUP=1"
exit

:cleanuptwo
cd %appdata%
if "%debug%" == "1" echo %~f0
if "%debug%" == "1" pause>nul|set/p =DEBUG-Delete %appdata%\officeinstallation next-Press any key to continue...
rmdir /q /s %appdata%\officeinstallation
if "%debug%" == "1" pause>nul|set/p =DEBUG-Delete %appdata%\deleteoldoffice next-Press any key to continue...
rmdir /q /s %appdata%\deleteoldoffice
if "%debug%" == "1" pause>nul|set/p =DEBUG-Delete %appdata%\3rikscript.bat next-Press any key to continue...
del %appdata%\3rikscript.bat
cls
echo  -----------------------------------------------------------------------------------------------
echo  Cleanup complete.
echo  -----------------------------------------------------------------------------------------------
pause
exit

:debugprompt
echo DEBUG MODE IS ON!
echo  -----------------------------------------------------------------------------------------------
echo Variables:
echo Called from a script?
if "%calledfromscript%" == "1" (echo Yes) else (echo No)
echo Prompt anyway enabled?
if "%promptanyway%" == "1" (echo Yes) else (echo No)
echo Scriptlocation set?
if "%scriptlocation%" == "" (echo No) else (echo Yes, value is %scriptlocation%)
echo Called by main script?
if "%calledfromscript1%" == "1" (echo Yes) else (echo No)
echo  -----------------------------------------------------------------------------------------------
echo   Press any key to go to directions
pause >nul
goto directions