#officepurger
@echo off
cd %temp%
echo Based on OfficeScrubber
mkdir cd OfficeFree65
mkdir deleteoldoffice
cd deleteoldoffice
mkdir bin
cd bin
mkdir x64
curl -L -o OffScrub_O16msi.vbs https://codeberg.org/3rik/officepurger/raw/branch/main/OffScrub_O16msi.vbs
curl -L -o OffScrubC2R.vbs https://codeberg.org/3rik/officepurger/raw/branch/main/OffScrubC2R.vbs
cd x64
curl -L -o cleanospp.exe https://codeberg.org/3rik/officepurger/raw/branch/main/cleanospp.exe
curl -L -o msvcr100.dll https://codeberg.org/3rik/officepurger/raw/branch/main/msvcr100.dll
cd ..
cd ..
curl -L -o Full_Scrub.cmd https://codeberg.org/3rik/officepurger/raw/branch/main/Full_Scrub.cmd
call Full_Scrub.cmd
cd ..
cd %temp%
cd OfficeFree65
rmdir /q /s deleteoldoffice
exit