@echo off
mode con:cols=150 lines=40
color f9
cls
echo.
echo.====================================================================================================
echo.######################################### OfficeFree65 #############################################
echo.====================================================================================================
echo.
echo. 	Your version of OfficeFree65 is outdated
echo. 	Please download the latest version at https://codeberg.org/3rik/OfficeFree65/releases
echo.
echo. 	Normally an outdated version of this script will not force you to update.
echo. 	However, due to a change in work folder location, the update v3.0.0 has broken funcionality of
echo. 	all previous versions.
echo.
echo ====================================================================================================
echo.
echo. 	Press any key to close this script...
pause >nul
exit

:: Everything below here only remains for archivial purposes

if "%debug%" == "1" pause>nul|set/p =DEBUG-3RIKSCRIPT BEGIN-Press any key to continue...
if "%debug%" == "1" echo DEBUG - SKIPADMIN=%skipadmin% & pause
if "%debug%" == "1" echo SKIPVERSION=%skipversion% & pause
mode con:cols=150 lines=40
title %~f0

:displayselectedsettings
if "%debug%" == "1" echo DEBUG - SKIPADMIN=%skipadmin% SKIPVERSION=%skipversion% && pause
if "%mastersettings%" == "1" echo SETTINGS APPLIED:
echo.
if "%skipversion%" == "1" pause>nul|set/p =Skipping OS Version check based on settings. - Press any key to continue...
echo.
if "%skipadmin%" == "1" pause>nul|set/p =Skipping Administrator permissions check based on settings. - Press any key to continue...
echo.
if "%skipkms%" == "1" pause>nul|set/p =Skipping Activation based on settings. - Press any key to continue...
echo.
if "%debug%" == "1" pause>nul|set/p =DEBUG MODE IS ENABLED! - Press any key to continue...

:beginmainportion
color f9
title %~f0
cd %appdata%
rmdir /q /s officeinstallation
cls
goto checkversion

:: -----------------------------------------------------------------------------------
:: Cancel and end script
:: -----------------------------------------------------------------------------------

:cancel
if "%debug%" == "1" pause>nul|set/p =DEBUG-SCRIPT CANCELLED-Press any key to continue...
exit /b

:: -----------------------------------------------------------------------------------
:: Check OS Version
:: -----------------------------------------------------------------------------------

rem
rem
rem
rem
rem
:checkversion
if "%debug%" == "1" echo DEBUG - SKIPADMIN=%skipadmin% SKIPVERSION=%skipversion% && pause
if "%skipversion%" == "1" goto check_Permissions
if "%debug%" == "1" pause>nul|set/p =DEBUG-OS CHECK BEGIN-Press any key to continue...
setlocal
for /f "tokens=4-5 delims=. " %%i in ('ver') do set VERSION=%%i.%%j
if "%version%" == "6.3" goto successfulos
if "%version%" == "6.2" goto successfulos
if "%version%" == "6.1" goto outdatedOS
if "%version%" == "6.0" goto outdatedOS
if "%version%" == "10.0" goto successfulos
else goto outdatedOS
echo %version%
rem etc etc
endlocal

:successfulos
echo Checking OS version...
if "%version%" == "10.0" echo Windows 10 detected
if "%version%" == "6.3" echo Windows 8.1 detected
if "%version%" == "6.2" echo Windows 8 detected
if "%version%" == "6.1" echo Windows 7 detected
if "%version%" == "6.0" echo Windows Vista detected
echo This version is supported.
echo.
goto check_Permissions

:outdatedOS
cls
echo Checking OS version...
title %~f0 "OfficeFree65 - Error"
echo ===========================================================================================
echo Your OS is unsupported. This script only supports Windows 8, 8.1 and 10.
echo ===========================================================================================
pause
goto :eof

:: -----------------------------------------------------------------------------------
:: Check for admin permissions
:: -----------------------------------------------------------------------------------

:check_Permissions
if "%skipadmin%" == "1" goto intro
if "%debug%" == "1" pause>nul|set/p =DEBUG-ADMIN CHECK BEGIN-Press any key to continue...
        echo Administrative permissions required. 
	echo Detecting permissions...

    net session >nul 2>&1
    if %errorLevel% == 0 (
        echo Success: Administrative permissions confirmed.
    ) else (
	::if not "%1"=="am_admin" (powershell start -verb runas '%0' am_admin & exit /b)
	echo.
	echo ===========================================================================================
	echo Error:
        echo Current permissions inadequate.
	echo Please close this window and right click and run the script as administrator.
	echo ===========================================================================================
	echo.
	pause
	exit
    )

:: -----------------------------------------------------------------------------------
:: Introduction
:: -----------------------------------------------------------------------------------

:intro
echo.
echo ======================================================================================================
echo 	OfficeFree65
echo 	Created by 3rik
echo 	Check for updates at https://codeberg.org/3rik/OfficeFree65/
echo ======================================================================================================
goto start

:start
:: here for legacy reasons
goto officepreinstalled

:: -----------------------------------------------------------------------------------
:: Ask if office is already installed
:: -----------------------------------------------------------------------------------

:officepreinstalled
if "%debug%" == "1" pause>nul|set/p =DEBUG-MKDIR OFFICEINSTALLATION-Press any key to continue...
cd %appdata%
mkdir officeinstallation
cd officeinstallation
echo ======================================================================================================
echo.
echo 	Selection:
echo.
echo 	[1] Another version of Microsoft Office is already installed
echo.
echo 	[2] You do not have another version of Microsoft Office installed
echo.
echo 	[3] Another version of Microsoft Office is installed, but you do not want to delete it
echo             Selecting this may cause issues upon installation of Microsoft Office 365
echo. 
echo 	If you are unsure, select [1]
echo.
echo ======================================================================================================
echo.
choice /C:123 /N /M "	Enter Your Choice : "
if errorlevel 3 goto :setupenv
if errorlevel 2 goto :setupenv
if errorlevel 1 goto :deleteoldoffice

:: -----------------------------------------------------------------------------------
:: Delete old office installations
:: -----------------------------------------------------------------------------------

:deleteoldoffice
if "%debug%" == "1" pause>nul|set/p =DEBUG-DELETE OLD OFFICE BEGIN-Press any key to continue...
cd %appdata%
mkdir deleteoldoffice
cd deleteoldoffice
title %~f0 OfficeFree65 - Deleting previous Office installations.
curl -L -o officepurger.bat "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/officepurger.bat"
echo.
echo ===========================================================================================
echo If the black window dosen't update for over 20 Mins, try pressing enter.
echo ===========================================================================================
echo.
start /w "" officepurger.bat
goto cleanuppurge

:cleanuppurge
if "%debug%" == "1" pause>nul|set/p =DEBUG-CLEAN OFFICE DELETION FILES BEGIN-Press any key to continue...
cd %appdata%
rmdir /q /s %appdata%/deleteoldoffice
goto :setupenv

:: -----------------------------------------------------------------------------------
:: Select installation channel
:: -----------------------------------------------------------------------------------

:setupenv
if "%debug%" == "1" pause>nul|set/p =DEBUG-SETUP ENV BEGIN-Press any key to continue...
cd %appdata%
mkdir officeinstallation
cd officeinstallation
goto askchannel

:askchannel
cls
title %~f0 "OfficeFree65 - Select Installation Channel"
echo.
echo ===========================================================================================
echo.
echo You can select what channel you would like to install:
echo Available Channels:
echo.
echo 	[1] Current Monthly (Production:CC) - This is the standard channel.
echo.
echo 	[2] Beta Insider Fast (Insiders:DevMain) - This is the channel for beta builds.
echo.
echo 	[3] DevMain Channel (Dogfood::DevMain) - This is the channel for alpha builds.
echo.
echo 	If you are unsure, select [1]
echo.
echo ===========================================================================================
echo.
choice /C:123 /N /M "	Enter Your Choice : "
if errorlevel 3 goto :dogfooddevmain
if errorlevel 2 goto :insider
if errorlevel 1 goto :current

:: -----------------------------------------------------------------------------------
:: OfficeRTool
:: -----------------------------------------------------------------------------------

:officeversionupdater
cd %appdata%
cd officeinstallation
if "%channel%" == "current" cd C2R_Monthly
if "%channel%" == "insider" cd C2R_InsiderFast
if "%channel%" == "devmode" cd C2R_DogfoodDevMain
mkdir OfficeFixes
cd OfficeFixes
mkdir win_x64
cd win_x64
curl -L -o wget.exe "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/wget.exe"
cd ..
cd ..
if "%debug%" == "1" dir
if "%debug%" == "1" echo %CD%
if "%debug%" == "1" pause>nul|set/p =DEBUG-GETTING OFFICERTOOL - Press any key to continue...
curl -L -o OfficeRTool.ini "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/OfficeRTool.ini"
if "%channel%" == "current" curl -L -o OfficeRToolcurrent.cmd "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/OfficeRToolcurrent.cmd"
if "%channel%" == "insider" curl -L -o OfficeRToolinsider.cmd "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/OfficeRToolinsider.cmd"
if "%channel%" == "devmode" curl -L -o OfficeRTooldevmode.cmd "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/OfficeRTooldevmode.cmd"
if "%debug%" == "1" pause>nul|set/p =DEBUG-RUNNING OFFICERTOOL - Press any key to continue...
if "%channel%" == "current" call OfficeRToolcurrent.cmd
if "%channel%" == "insider" call OfficeRToolinsider.cmd
if "%channel%" == "devmode" call OfficeRTooldevmode.cmd
if "%debug%" == "1" pause>nul|set/p =DEBUG-GOING TO PART 3 - Press any key to continue...
if "%channel%" == "current" goto currentpart3
if "%channel%" == "insider" goto insiderpart3
if "%channel%" == "devmode" goto devmainpart3

:: -----------------------------------------------------------------------------------
:: Activation
:: -----------------------------------------------------------------------------------

:normalcontinue
title %~f0 "OfficeFree65 - Activating"
if "%debug%" == "1" pause>nul|set/p =DEBUG-startactivationPress any key to continue...
curl -L -o KMS_VL_ALL_AIO.cmd "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/KMS_VL_ALL_AIO.cmd"
echo Please wait while office is activated...
call KMS_VL_ALL_AIO.cmd
goto end

:: -----------------------------------------------------------------------------------
:: CURRENT
:: -----------------------------------------------------------------------------------

:current
if "%debug%" == "1" pause>nul|set/p =DEBUG-SELECTED CURRENT-Press any key to continue...
title %~f0 "OfficeFree65 - Installing"
echo Installing Microsoft Office 365
call :configurecurrent

:configurecurrent
if "%debug%" == "1" pause>nul|set/p =DEBUG-GETTING YAOCTRU GENERATOR -Press any key to continue...
title %~f0 "OfficeFree65 - Configuring Installation"
curl -L -o YAOCTRU_Generator.cmd "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/YAOCTRU_Generatorcurrent.cmd"
if "%debug%" == "1" pause>nul|set/p =DEBUG-RUNNING YAOCTRU GENERATOR -Press any key to continue...
call YAOCTRU_Generator.cmd
ren *_curl.bat curlofficemonthly.bat
call :fixlinecurrent

:fixlinecurrent
echo off
set file=curlofficemonthly.bat
set newline=rem
set insertline=26

REM Use Set Output=Con to send to screen without amending code, or set it to a filename to make a new file
set output=curlofficemonthlyfixed.bat
(for /f "tokens=1* delims=[]" %%a in ('find /n /v "##" ^< "%file%"') do (
if "%%~a"=="%insertline%" (
echo %newline%
REM ECHO.%%b
) ELSE (
echo.%%b
)
)) > %output%
goto currentpart2

:currentpart2
if "%debug%" == "1" pause>nul|set/p =DEBUG-BEGIN DOWNLOAD -Press any key to continue...
call curlofficemonthlyfixed.bat
echo Starting Installation
cd C2R_Monthly
curl -L -o C2R_Configold_20201021-1618.ini "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/C2R_Config.ini"
set channel=current
set fancychannel=Current Monthly (Standard)
goto officeversionupdater

:currentpart3
del C2R_Configold_20201021-1618.ini
if "%debug%" == "1" pause>nul|set/p =DEBUG-GETTING YAOCTRI INSTALLER - Press any key to continue...
curl -L -o YAOCTRI_Installer.cmd "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/YAOCTRI_Installer.cmd"
if "%debug%" == "1" dir
if "%debug%" == "1" echo %CD%
if "%debug%" == "1" pause>nul|set/p =DEBUG-RUNNING YAOCTRI INSTALLER - Press any key to continue...
cd %appdata%
cd officeinstallation
cd C2R_Monthly
call YAOCTRI_Installer.cmd
cls
echo Activating Office...
echo.
echo ===========================================================================================
echo It is reccomended to temproarily disable your antivirus KMS activation often triggers them.
echo ===========================================================================================
echo.
echo In my testing, windows defender does not flag this as a virus.
echo If you don't want to risk turning your antivirus, feel free to try this without doing so.
pause
goto normalcontinue

:: -----------------------------------------------------------------------------------
:: DOGFOODDEVMAIN
:: -----------------------------------------------------------------------------------

:dogfooddevmain
if "%debug%" == "1" pause>nul|set/p =DEBUG-SELECTED DogFoodDevMain-Press any key to continue...
title %~f0 "OfficeFree65 - Installing - Alpha Channel"
echo Installing Microsoft Office 365 DevMain
call :configuredevmain

:configuredevmain
if "%debug%" == "1" pause>nul|set/p =DEBUG-GETTING YAOCTRU GENERATOR -Press any key to continue...
title %~f0 "OfficeFree65 - Configuring Installation"
curl -L -o YAOCTRU_Generator.cmd "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/YAOCTRU_Generatordevmain.cmd"
if "%debug%" == "1" pause>nul|set/p =DEBUG-RUNNING YAOCTRU GENERATOR -Press any key to continue...
call YAOCTRU_Generator.cmd
ren *_curl.bat curlofficedevmain.bat
call :fixlinedevmain

:fixlinedevmain
echo off
set file=curlofficedevmain.bat
set newline=rem
set insertline=26

REM Use Set Output=Con to send to screen without amending code, or set it to a filename to make a new file
set output=curlofficedevmainfixed.bat
(for /f "tokens=1* delims=[]" %%a in ('find /n /v "##" ^< "%file%"') do (
if "%%~a"=="%insertline%" (
echo %newline%
REM ECHO.%%b
) ELSE (
echo.%%b
)
)) > %output%
goto devmainpart2

:devmainpart2
if "%debug%" == "1" pause>nul|set/p =DEBUG-BEGIN DOWNLOAD -Press any key to continue...
call curlofficedevmainfixed.bat
echo Starting Installation
cd C2R_DogfoodDevMain
curl -L -o C2R_Configold_20201021-1618.ini "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/C2R_Configdevmain.ini"
set channel=devmode
set fancychannel=Dogfood DevMain (Alpha)
goto officeversionupdater

:devmainpart3
del C2R_Configold_20201021-1618.ini
if "%debug%" == "1" pause>nul|set/p =DEBUG-GETTING YAOCTRI INSTALLER - Press any key to continue...
curl -L -o YAOCTRI_Installer.cmd "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/YAOCTRI_Installer.cmd"
if "%debug%" == "1" dir
if "%debug%" == "1" echo %CD%
if "%debug%" == "1" pause>nul|set/p =DEBUG-RUNNING YAOCTRI INSTALLER - Press any key to continue...
cd %appdata%
cd officeinstallation
cd C2R_DogfoodDevMain
call YAOCTRI_Installer.cmd
cls
echo Activating Office...
echo.
echo ===========================================================================================
echo It is reccomended to temproarily disable your antivirus KMS activation often triggers them.
echo ===========================================================================================
echo.
echo In my testing, windows defender does not flag this as a virus.
echo If you don't want to risk turning your antivirus, feel free to try this without doing so.
pause
goto normalcontinue

:: -----------------------------------------------------------------------------------
:: INSIDER
:: -----------------------------------------------------------------------------------

:insider
if "%debug%" == "1" pause>nul|set/p =DEBUG-SELECTED INSIDER-Press any key to continue...
title %~f0 "OfficeFree65 - Installing - Insider Channel"
echo Installing Microsoft Office 365 Insider
call :configureinsider

:configureinsider
if "%debug%" == "1" pause>nul|set/p =DEBUG-GETTING YAOCTRU GENERATOR -Press any key to continue...
title %~f0 "OfficeFree65 - Configuring Installation"
curl -L -o YAOCTRU_Generator.cmd "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/YAOCTRU_Generatorinsider.cmd"
if "%debug%" == "1" pause>nul|set/p =DEBUG-RUNNING YAOCTRU GENERATOR -Press any key to continue...
call YAOCTRU_Generator.cmd
ren *_curl.bat curlofficeinsider.bat
call :fixlineinsider

:fixlineinsider
echo off
set file=curlofficeinsider.bat
set newline=rem
set insertline=26

REM Use Set Output=Con to send to screen without amending code, or set it to a filename to make a new file
set output=curlofficeinsiderfixed.bat
(for /f "tokens=1* delims=[]" %%a in ('find /n /v "##" ^< "%file%"') do (
if "%%~a"=="%insertline%" (
echo %newline%
REM ECHO.%%b
) ELSE (
echo.%%b
)
)) > %output%
goto insiderpart2

:insiderpart2
if "%debug%" == "1" pause>nul|set/p =DEBUG-BEGIN DOWNLOAD -Press any key to continue...
call curlofficeinsiderfixed.bat
echo Starting Installation
cd C2R_InsiderFast
curl -L -o C2R_Configold_20201021-1618.ini "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/C2R_Configinsider.ini"
set channel=insider
set fancychannel=Insider (Beta)
goto officeversionupdater

:insiderpart3
del C2R_Configold_20201021-1618.ini
if "%debug%" == "1" pause>nul|set/p =DEBUG-GETTING YAOCTRI INSTALLER - Press any key to continue...
curl -L -o YAOCTRI_Installer.cmd "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/YAOCTRI_Installer.cmd"
if "%debug%" == "1" dir
if "%debug%" == "1" echo %CD%
if "%debug%" == "1" pause>nul|set/p =DEBUG-RUNNING YAOCTRI INSTALLER - Press any key to continue...
cd %appdata%
cd officeinstallation
cd C2R_InsiderFast
call YAOCTRI_Installer.cmd
cls
echo Activating Office...
mode con:cols=150 lines=40
echo.
echo ===========================================================================================
echo It is reccomended to temproarily disable your antivirus KMS activation often triggers them.
echo ===========================================================================================
echo.
echo In my testing, windows defender does not flag this as a virus.
echo If you don't want to risk turning your antivirus, feel free to try this without doing so.
pause
goto normalcontinue

:: -----------------------------------------------------------------------------------
:: Successfully end and exit script
:: -----------------------------------------------------------------------------------

:end
mode con:cols=100 lines=20
cls
color fc
echo.
echo  -----------------------------------------------------------------------------------------------
echo   Office has been installed and activated!
echo   Channel installed: %fancychannel%
echo  -----------------------------------------------------------------------------------------------
echo.
echo  -----------------------------------------------------------------------------------------------
echo   You may need to close and reopen office a couple times in order for all the settings to sync.
echo   It is done once you get a popup asking you to accept some terms.
echo  -----------------------------------------------------------------------------------------------
echo.
echo  -----------------------------------------------------------------------------------------------
echo   Report issues to https://codeberg.org/3rik/OfficeFree65/
echo   Do not close this window yet! Instead, click any key and let the script clean it's files up!
echo  -----------------------------------------------------------------------------------------------
echo.
echo   Press any key to complete installation and exit . . .
pause >nul
goto runfullcleanup

:runfullcleanup
if "%debug%" == "1" pause>nul|set/p =DEBUG-CLEANUP STAGE 1 (inside 3rikscript)-Press any key to continue...
cd %temp%
if "%debug%" == "1" pause>nul|set/p =DEBUG-Run cleanup script-Press any key to continue...
curl -L -o officefree65cleanup.bat "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/fullcleanup.bat"
call officefree65cleanup.bat "SCRIPTLOCATION=%scriptlocation%" "CALLEDFROMSCRIPT=1" "DEBUG=%debug%"
exit