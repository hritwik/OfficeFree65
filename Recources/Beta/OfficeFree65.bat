@echo off
set scriptversion=300
:startofscript
if "%calledfrommainscript%" == "1" goto :end
:: -----------------------------------------------------------------------------------
:: NORMALLY THERE IS NO NEED TO CHANGE ANYTHING ABOVE HERE!
:: -----------------------------------------------------------------------------------
::    _____ ______ _______ _______ _____ _   _  _____  _____     
::   / ____|  ____|__   __|__   __|_   _| \ | |/ ____|/ ____|   _ 
::  | (___ | |__     | |     | |    | | |  \| | |  __| (___    (_)    
::   \___ \|  __|    | |     | |    | | | . ` | | |_ |\___ \ 
::   ____) | |____   | |     | |   _| |_| |\  | |__| |____) |   _ 
::  |_____/|______|  |_|     |_|  |_____|_| \_|\_____|_____/   (_)
:: OfficeFree65 v2.0.0 by 3rik https://codeberg.org/3rik/OfficeFree65/
:: -----------------------------------------------------------------------------------
:: 
:: Master Settings Toggle
:: 1 = Ignore settings below, apply settings from downloaded script (3rikscript.bat)
:: 2 = Apply settings below, ignore settings from downloaded script (3rikscript.bat)
:: Unless you have a custom 3rikscript.bat set and want to use it's settings, set this to 2.
:: Default: 2
:: 
    set mastersettings=2
::
:: -----------------------------------------------------------------------------------
:: DO NOT CHANGE THiS LINE!:
if "%mastersettings%" == "1" goto checkforupdates
:: -----------------------------------------------------------------------------------
:: Additional toggles. Default for these is 0.
:: -----------------------------------------------------------------------------------
::
:: Skip OS Version check. 1 - ON. Any other number - OFF
::
    set skipversioncheck=0
::
:: Skip Administrator permissions check. 1 - ON. Any other number - OFF.
::
    set skipadmincheck=0
::
:: Run full cleanup of script work files (without installation etc) 1 - ON. Any other number - OFF.
:: Bypassess all other settings
::
    set runfullcleanup=0
::
:: Uninstall Office
:: Jump straight to the office purger and delete all installations of office, including
:: Office 365 installed by this script.
:: 1 = Enabled. 2 = Disabled. Default - Disabled.
::
    set runofficepurger=0
::
:: -----------------------------------------------------------------------------------
:: Advanced Settings. Default for these is 0.
:: -----------------------------------------------------------------------------------
:: Debug Mode. 1 - ON. Any other number - OFF
    set debug=0
:: :afterdebug
:: Use custom 3rikscript.bat 1=ON. Any other number = OFF.
    set customscript=0
:: If you are using a custom script, set it's location here:
    set customScriptLocation="C:\Users\erik2\Documents\RewriteOfficeFree65\mainscript.bat
:: -----------------------------------------------------------------------------------
:: Betas. Default for these is 0
:: -----------------------------------------------------------------------------------
:: To enable the beta source for 3rikscript.bat (WARNING: MAY BE UNSTABLE)
:: 1 = enabled. Other numbers = disabled. Default: DISABLED
    set betascript=0
:: -----------------------------------------------------------------------------------
:: NORMALLY THERE IS NO NEED TO CHANGE ANYTHING BELOW HERE!
:: -----------------------------------------------------------------------------------
goto :checkforupdates

:checkforupdates
cd %temp%
curl -L -o officefree65latestversion.txt "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/latestversion.txt"
>nul find "%scriptversion%" officefree65latestversion.txt && (
  goto :loadvars
) || (
  goto :updateavailable
)

:updateavailable
mode con:cols=150 lines=40
color f9
CLS
echo.========================================================================================
echo.################################### OfficeFree65 #######################################
echo.========================================================================================
echo.
echo.  This version of OfficeFree65 is outdated!
echo.  Download the latest version at https://codeberg.org/3rik/OfficeFree65/releases
echo.  Being outdated, some parts of this script may no longer work properly
echo.  
echo.========================================================================================
echo.
echo.  Press any key to continue using this oudated version...
pause >nul
goto loadvars

:loadvars
del %temp%\officefree65latestversion.txt
if "%debug%" == "1" echo. & pause>nul|set/p =[DEBUG] Loading variables
mode con:cols=150 lines=40
color f9
set launcherscriptlocation=%0
set launcherscriptpath=%~dp0
goto :checkadminsettings

:checkadminsettings
if "%skipadmincheck%" == "1" goto :loadsettings
:: Get admin rights
if "%debug%" == "1" echo DEBUG start get admin && pause
if "%skipadmin%" == "1" goto gotadmin
CLS
ECHO.
ECHO =============================
ECHO Running Admin shell
ECHO =============================

:init
setlocal DisableDelayedExpansion
set "batchPath=%~0"
for %%k in (%0) do set batchName=%%~nk
set "vbsGetPrivileges=%temp%\OEgetPriv_%batchName%.vbs"
setlocal EnableDelayedExpansion

:checkPrivileges
NET FILE 1>NUL 2>NUL
if '%errorlevel%' == '0' ( goto gotPrivileges ) else ( goto getPrivileges )

:getPrivileges
if '%1'=='ELEV' (echo ELEV & shift /1 & goto gotPrivileges)
ECHO.
ECHO **************************************
ECHO Invoking UAC for Privilege Escalation
ECHO **************************************

ECHO Set UAC = CreateObject^("Shell.Application"^) > "%vbsGetPrivileges%"
ECHO args = "ELEV " >> "%vbsGetPrivileges%"
ECHO For Each strArg in WScript.Arguments >> "%vbsGetPrivileges%"
ECHO args = args ^& strArg ^& " "  >> "%vbsGetPrivileges%"
ECHO Next >> "%vbsGetPrivileges%"
ECHO UAC.ShellExecute "!batchPath!", args, "", "runas", 1 >> "%vbsGetPrivileges%"
"%SystemRoot%\System32\WScript.exe" "%vbsGetPrivileges%" %*
exit /B

:gotPrivileges
setlocal & pushd .
cd /d %~dp0
if '%1'=='ELEV' (del "%vbsGetPrivileges%" 1>nul 2>nul  &  shift /1)

:loadsettings
title OfficeFree65 - Getting main script
if "%debug%" == "1" echo. & pause>nul|set/p =[DEBUG] Loading settings
setlocal EnableExtensions
setlocal EnableDelayedExpansion
if "%customscript%" == "1" goto :loadcustomscript
if "%betascript%" == "1" goto :loadbetascript
goto loadmainscript

:getadminperms
if "%skipadmincheck%" == "1" goto selectinstallationchannel

:loadmainscript
if "%debug%" == "1" echo. & pause>nul|set/p =[DEBUG] called loadmainscript
cd %temp%
mkdir OfficeFree65 >nul 2>&1
cd OfficeFree65
curl -o mainscript.bat "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/mainscript.bat"
call mainscript.bat
goto :exit

:loadbetascript
if "%debug%" == "1" echo. & pause>nul|set/p =[DEBUG] called loadbetascript
cd %temp%
mkdir OfficeFree65 >nul 2>&1
cd OfficeFree65
curl -o mainscript.bat "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/Beta/mainscript.bat"
echo.========================================================================================
echo.################################### OfficeFree65 #######################################
echo.========================================================================================
echo.
echo.  You are about to run a beta script. This may be an older version than the regular
echo.  version, if there has not been a new beta script releases since.
echo.  Some parts of this script may not work properly.
echo.  
echo.========================================================================================
pause
call mainscript.bat
goto :exit

:loadcustomscript
if "%debug%" == "1" echo. & pause>nul|set/p =[DEBUG] called loadcustomscript
cd %temp%
mkdir OfficeFree65 >nul 2>&1
cd OfficeFree65
echo.========================================================================================
echo.################################### OfficeFree65 #######################################
echo.========================================================================================
echo.
echo.  You are about to run a custom script.
echo.  You can disable this by editing this file at %0
echo.  and changing "set customscript=1" to "set customscript=0" in the settings portion
echo.  at the top of the file.
echo.  You will need to accept the EULA twice. Once now, and, unless you have removed it,
echo.  once again when your custom script starts.
echo.  Some parts of this script may not work properly.
echo.  
echo.========================================================================================
pause
goto promptifbegin

:promptifbegin
if "%debug%" == "1" echo. & pause>nul|set/p =[DEBUG] called promptifbegin
cls
echo.==============================================================================
echo.############################ OfficeFree65 -- EULA ############################
echo.==============================================================================
echo.
echo.
echo. OfficeFree65 will install and activate Microsoft Office 365 on your
echo. computer.
echo. 
echo. Created by 3rik and available at https://codeberg.org/3rik/OfficeFree65/
echo. and https://github.com/3rikk/officefree65/
echo. 
echo.
echo. OfficeFree65 is provided 'as-is', without any express or implied warranty.
echo. In no event will the author be held liable for any damages arising from the
echo. use of this script.
echo.
echo. OfficeFree65 uses various 3rd party tools to accomplish some of its
echo. tasks.
echo.
echo. By Selecting Y for Yes, you agree to these terms and want to begin
echo. installation.
echo.
if "%debug%" == "1" set areanysettingsenabled=1
if "%skipadmincheck%" == "1" set areanysettingsenabled=1
if "%skipversioncheck%" == "1" set areanysettingsenabled=1
if "%areanysettingsenabled%" == "1" echo.==============================================================================
if "%areanysettingsenabled%" == "1" echo.    Special Settings:
if "%areanysettingsenabled%" == "1" echo.
if "%debug%" == "1" echo.    Debug Mode has been enabled in settings.
if "%skipadmincheck%" == "1" echo.    Skipping administrator permissions check has been enabled in settings.
if "%skipversioncheck%" == "1" echo.    Skipping OS Version compatibility check has been enabled in settings.
echo.
echo.==============================================================================
choice /C YN /N /M "###############################[ 'Y'es/ 'N'o ]################################"
if errorlevel 1 goto callcustomscript
if errorlevel 2 goto end

:callcustomscript
call %customScriptLocation%
goto :exit

:end
if "%debug%" == "1" echo. & pause>nul|set/p =[DEBUG] called end
rmdir /q /s %temp%\OfficeFree65
endlocal
exit