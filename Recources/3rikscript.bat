@echo off
mode con:cols=150 lines=40
color f9
cls
echo.
echo.====================================================================================================
echo.######################################### OfficeFree65 #############################################
echo.====================================================================================================
echo.
echo. 	Your version of OfficeFree65 is outdated
echo. 	Please download the latest version at https://codeberg.org/3rik/OfficeFree65/releases
echo.
echo. 	Normally an outdated version of this script will not force you to update.
echo. 	However, due to a change in work folder location, the update v3.0.0 has broken funcionality of
echo. 	all previous versions.
echo.
echo ====================================================================================================
echo.
echo. 	Press any key to close this script...
pause >nul
exit

:: Everything below here only remains for archivial purposes

title "OfficeFree65"
mode con:cols=150 lines=40
color 17
cd %appdata%
rmdir officeinstallation
goto online

:online
# ##Export to online version from here onwards##
# ##(Thats for myself, dont worry if you have no idea what I mean)##
cls

:checkversion
setlocal
for /f "tokens=4-5 delims=. " %%i in ('ver') do set VERSION=%%i.%%j
if "%version%" == "6.3" goto successfulos
if "%version%" == "6.2" goto successfulos
if "%version%" == "6.1" goto outdatedOS
if "%version%" == "6.0" goto outdatedOS
if "%version%" == "10.0" goto successfulos
else goto outdatedOS
echo %version%
rem etc etc
endlocal

:successfulos
echo OS Version Check Successful
goto check_Permissions

:outdatedOS
cls
title "OfficeFree65 - Error"
echo ===========================================================================================
echo Your OS is unsupported. This script only supports Windows 8, 8.1 and 10.
echo ===========================================================================================
pause
goto :eof

:check_Permissions
        echo Administrative permissions required. 
	echo Detecting permissions...

    net session >nul 2>&1
    if %errorLevel% == 0 (
        echo Success: Administrative permissions confirmed.
    ) else (
	echo.
	echo ===========================================================================================
	echo Error:
        echo Current permissions inadequate.
	echo Please close this window and right click and run the script as administrator.
	echo ===========================================================================================
	echo.
	pause
	exit
    )

:intro
echo.
echo ===========================================================================================
echo Latest Version (Online Mode)
echo Created by 3rik
echo made with scripts by abbodi1406 (https://github.com/abbodi1406) and KMS Activator
echo Always make sure you get this from the official source.
echo Fake versions could have malicious code implanted into them.
echo The original script is available at https://github.com/3rikk/officefree65/ 
echo and https://codeberg.org/3rik/OfficeFree65/
echo ===========================================================================================

:start
ECHO.
set choice=
set /p choice=Type 1 and hit enter to start installation, or press 2 to cancel it.  
if not '%choice%'=='' set choice=%choice:~0,1%
if '%choice%'=='1' goto officepreinstalled
if '%choice%'=='2' goto cancel
if '%choice%'=='69' goto easteregg
ECHO "%choice%" is not valid, try again
ECHO.
goto start

:officepreinstalled
mkdir officeinstallation
cd officeinstallation
ECHO.
set officepreinstalled=
echo ===========================================================================================
echo.
set /p officepreinstalled=Press 1 and hit enter if you already have another version of Office installed. Press 2 if you do not.  
echo ===========================================================================================
if not '%officepreinstalled%'=='' set officepreinstalled=%officepreinstalled:~0,1%
if '%officepreinstalled%'=='1' goto deleteoldoffice
if '%officepreinstalled%'=='2' goto askchannel
ECHO "%officepreinstalled%" is not valid, try again
ECHO.
goto start

:cancel
exit /b

:deleteoldoffice
title "OfficeFree65 - Deleting previous Office installations."
curl -L -o officepurger.bat "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/officepurger.bat"
echo.
echo ===========================================================================================
echo If window dosen't update for over 20 Mins, try pressing enter.
echo ===========================================================================================
echo.
start /w "" officepurger.bat
goto cleanuppurge

:purgeprompt
echo ===========================================================================================
echo ONLY CONTINUE ONCE THE NEWLY CREATED BLACK WINDOW HAS CLOSED!
echo ===========================================================================================
echo.
set afterpurger=
set /p afterpurger=Type "DONE" to continue. 
if not '%afterpurger%'=='' set afterpurger=%afterpurger:~0,1%
if '%afterpurger%'=='D' goto cleanuppurge
ECHO "%afterpurger%" is not valid, try again
ECHO.
goto purgeprompt

:cleanuppurge
rmdir /q /s %appdata%/deleteoldoffice
goto :askchannel

:continue
title "OfficeFree65 - Installing"
echo Installing Microsoft Office 365
call :configurecurrent

:currentpart2
call curlofficemonthlyfixed.bat
echo Starting Installation
cd C2R_Monthly
curl -L -o C2R_Config_20201021-1618.ini "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/C2R_Config.ini"
curl -L -o YAOCTRI_Installer.cmd "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/YAOCTRI_Installer.cmd"
call YAOCTRI_Installer.cmd
cls
echo Activating Office...
echo.
echo ===========================================================================================
echo It is reccomended to temproarily disable your antivirus KMS activation often triggers them.
echo ===========================================================================================
echo.
echo In my testing, windows defender does not flag this as a virus.
echo If you don't want to risk turning your antivirus, feel free to try this without doing so.
pause
goto normalcontinue

:antiviruscheck
set antiviruscheck=
cls
echo ===========================================================================================
set /p antiviruscheck=Press 1 and hit enter if you want to try without disabling it, Press 2 if you have disabled it.
echo ===========================================================================================    
if not '%antiviruscheck%'=='' set antiviruscheck=%antiviruscheck:~0,1%
if '%antiviruscheck%'=='1' goto attemptwithoutdisable
if '%antiviruscheck%'=='2' goto normalcontinue
ECHO "%antiviruscheck%" is not valid, try again
ECHO.
goto antiviruscheck

:attemptwithoutdisable
cls
echo This has not been implemented yet. 
echo Continuing the normal process. 
echo If it fails, disable your antivirus, delete the "officeinstallation" folder and try again.
pause
goto normalcontinue

:askchannel
cls
title "OfficeFree65"
echo ===========================================================================================
echo You can select what channel you would like to install:
echo Available Channels:
echo 1. Current Monthly (Production:CC) - This is the standard channel.
echo 2. Beta Insider Fast (Insiders:DevMain) - This is the channel for beta builds.
echo 3. DevMain Channel (Dogfood::DevMain) - This is the channel for alpha builds.
echo.
echo If you are unsure, select 1.
echo.
echo ===========================================================================================
set askchannel=
set /p askchannel=Enter the number of the channel you want to use. 
if not '%aaskchannel%'=='' set askchannel=%askchannel:~0,1%
if '%askchannel%'=='1' goto continue
if '%askchannel%'=='2' goto insider
if '%askchannel%'=='3' goto dogfooddevmain
ECHO "%askchannel%" is not valid, try again
ECHO.
goto askchannel

:dogfooddevmain
echo Installing Microsoft Office 365 DevMain
call :configuredevmain

:insider
echo Installing Microsoft Office 365 Insider
call :configureinsider

:insiderpart2
call curlofficeinsiderfixed.bat
echo Starting Installation
cd C2R_InsiderFast
curl -L -o C2R_Config_20201021-1618.ini "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/C2R_Configinsider.ini"
curl -L -o YAOCTRI_Installer.cmd "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/YAOCTRI_Installer.cmd"
call YAOCTRI_Installer.cmd
cls
echo Activating Office...
echo ===========================================================================================
echo It is reccomended to temproarily disable your antivirus KMS activation often triggers them.
echo ===========================================================================================
echo In my testing, windows defender does not flag this as a virus.
echo If you don't want to risk turning your antivirus, feel free to try this without doing so.
pause
goto normalcontinue

:devmainpart2
call curlofficedevmainfixed.bat
echo Starting Installation
cd C2R_DogfoodDevMain
curl -L -o C2R_Config_20201021-1618.ini "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/C2R_Configdevmain.ini"
curl -L -o YAOCTRI_Installer.cmd "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/YAOCTRI_Installer.cmd"
call YAOCTRI_Installer.cmd
cls
echo Activating Office...
echo ===========================================================================================
echo It is reccomended to temproarily disable your antivirus KMS activation often triggers them.
echo ===========================================================================================
echo In my testing, windows defender does not flag this as a virus.
echo If you don't want to risk turning your antivirus, feel free to try this without doing so.
pause
goto normalcontinue

:askcomponents
echo Not implemented yet.
pause
goto online

:configuredevmain
title "OfficeFree65 - Configuring Installation"
curl -L -o YAOCTRU_Generator.cmd "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/YAOCTRU_Generatordevmain.cmd"
call YAOCTRU_Generator.cmd
ren *_curl.bat curlofficedevmain.bat
call :fixlinedevmain

:configurecurrent
title "OfficeFree65 - Configuring Installation"
curl -L -o YAOCTRU_Generator.cmd "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/YAOCTRU_Generatorcurrent.cmd"
call YAOCTRU_Generator.cmd
ren *_curl.bat curlofficemonthly.bat
call :fixlinecurrent

:configureinsider
title "OfficeFree65 - Configuring Installation"
curl -L -o YAOCTRU_Generator.cmd "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/YAOCTRU_Generatorinsider.cmd"
call YAOCTRU_Generator.cmd
ren *_curl.bat curlofficeinsider.bat
call :fixlineinsider

:easteregg
cls
echo .
echo 69 420 69 420 69 420 69 420 69 420 69 420 69 420 69 420 69 420 69 420 69 420 69 420
echo nice
echo 69 420 69 420 69 420 69 420 69 420 69 420 69 420 69 420 69 420 69 420 69 420 69 420
echo .
pause
goto start

:normalcontinue
title "OfficeFree65 - Activating"
curl -L -o KMS_VL_ALL_AIO.cmd "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/KMS_VL_ALL_AIO.cmd"
echo Please wait while office is activated...
call KMS_VL_ALL_AIO.cmd
goto end

:fixlineinsider
echo off
set file=curlofficeinsider.bat
set newline=rem
set insertline=26

REM Use Set Output=Con to send to screen without amending code, or set it to a filename to make a new file
set output=curlofficeinsiderfixed.bat
(for /f "tokens=1* delims=[]" %%a in ('find /n /v "##" ^< "%file%"') do (
if "%%~a"=="%insertline%" (
echo %newline%
REM ECHO.%%b
) ELSE (
echo.%%b
)
)) > %output%
goto insiderpart2

:fixlinedevmain
echo off
set file=curlofficedevmain.bat
set newline=rem
set insertline=26

REM Use Set Output=Con to send to screen without amending code, or set it to a filename to make a new file
set output=curlofficedevmainfixed.bat
(for /f "tokens=1* delims=[]" %%a in ('find /n /v "##" ^< "%file%"') do (
if "%%~a"=="%insertline%" (
echo %newline%
REM ECHO.%%b
) ELSE (
echo.%%b
)
)) > %output%
goto devmainpart2

:fixlinecurrent
echo off
set file=curlofficemonthly.bat
set newline=rem
set insertline=26

REM Use Set Output=Con to send to screen without amending code, or set it to a filename to make a new file
set output=curlofficemonthlyfixed.bat
(for /f "tokens=1* delims=[]" %%a in ('find /n /v "##" ^< "%file%"') do (
if "%%~a"=="%insertline%" (
echo %newline%
REM ECHO.%%b
) ELSE (
echo.%%b
)
)) > %output%
goto currentpart2

:end
cls
color 0a
echo Office has been installed and activated!
echo You may need to close and reopen office a couple times in order for all the settings to sync.
echo It is done once you get a popup asking you to accept some terms.
pause
rmdir /q /s %appdata%\deleteoldoffice
forfiles /P %appdata%\officeinstallation /M * /C "cmd /c if @isdir==FALSE del @file"
forfiles /P %appdata%\officeinstallation /M * /C "cmd /c if @isdir==TRUE rmdir /S /Q @file"
rmdir /q /s %appdata%\officeinstallation
exit