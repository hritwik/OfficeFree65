@echo off
:startofscript
set loadactivationfromutilities=0
goto readsettings

:readsettings
if "%debug%" == "1" echo. & pause>nul|set/p =[DEBUG] called readsettings
if "%runfullcleanup%" == "1" goto :cleanoldtempfiles
if "%runofficepurger%" == "1" goto utilitydeleteoldofficeinstallations
if "%debug%" == "1" echo. & pause>nul|set/p =[DEBUG] Started mainscript.bat
title OfficeFree65
color fc
if "%skipadmincheck%" == "1" echo.
goto promptifbegin

:promptifbegin
if "%skipeulaprompt%" == "1" goto checkosversion
if "%debug%" == "1" echo. & pause>nul|set/p =[DEBUG] called promptifbegin
cls
echo.==================================================================================
echo.############################## OfficeFree65 -- EULA ##############################
echo.==================================================================================
echo.
echo.
echo.==================================================================================
echo.==================================================================================
echo. -----WARNING
echo.
echo. Development on this project has been temproarily paused.
echo. Activation will likely fail.
echo. You can manually activate after using this script by downloading and running
echo. https://github.com/abbodi1406/KMS_VL_ALL_AIO/releases/
echo. And pressing "5" then "2" on the menu to activate.
echo.
echo.==================================================================================
echo.==================================================================================
echo.
echo.
echo. OfficeFree65 will install and activate Microsoft Office 365 on your computer.
echo. 
echo. Created by 3rik and available at https://codeberg.org/3rik/OfficeFree65/
echo. and https://github.com/3rikk/officefree65/
echo. 
echo. OfficeFree65 is provided 'as-is', without any express or implied warranty.
echo. In no event will the author be held liable for any damages arising from the use
echo. of this script.
echo.
echo. OfficeFree65 uses various 3rd party tools to accomplish some of its tasks.
echo.
echo. By Selecting Y for Yes, you agree to these terms and want to begin installation.
echo.
if "%debug%" == "1" echo.==================================================================================
if "%debug%" == "1" echo [DEBUG] OSVER %osversion%, FOSVER %fancyosversion%
if "%debug%" == "1" echo [DEBUG] OPTUPD %optupdateavailable%, UPD %updateavailable%, SKPEULA %skipeulaprompt%, SKPVRCHK %skipversioncheck%, SKPADMCHK %skipadmincheck%
if "%debug%" == "1" echo [DEBUG] AUTOC %automaticchannel%, ADO %automaticdeleteoldofficeinstallations%, SKPAV %skipantiviruswarning%, SKPOSP %skipotherscriptprompt%, SKPUPDA %skipupdateavailable%, IGNOPTUPD %ignoreoptionalupdate% 
if "%debug%" == "1" echo [DEBUG] CUSTSCRPT %customscript%, BSCRPT %betascript%, MSTR %mastersettings%
if "%debug%" == "1" echo [DEBUG] OPTUPDW %optupdateavailable%, UPDW %updateavailable%, BASESCRPTVER %basescriptversion%, OPTSCRPTVER %optscriptversion%
if "%debug%" == "1" echo [DEBUG] LAUNCHSCRPTLOC %launcherscriptlocation%
if "%debug%" == "1" echo [DEBUG] LAUNCHSCRPTPATH %launcherscriptpath%
if "%optupdateavailable%" == "yes" echo.==================================================================================
if "%optupdateavailable%" == "yes" echo.  An optional update is available. If you encounter errors, please try updating.
if "%optupdateavailable%" == "yes" echo.
if "%updateavailable%" == "yes" echo.==================================================================================
if "%updateavailable%" == "yes" echo.      An update is available. If you encounter errors, please try updating.
if "%updateavailable%" == "yes" echo.
if "%debug%" == "1" set areanysettingsenabled=1
if "%skipadmincheck%" == "1" set areanysettingsenabled=1
if "%skipversioncheck%" == "1" set areanysettingsenabled=1
if "%areanysettingsenabled%" == "1" echo.==================================================================================
if "%areanysettingsenabled%" == "1" echo.      Special Settings:
if "%areanysettingsenabled%" == "1" echo.
if "%debug%" == "1" echo.      Debug Mode has been enabled in settings.
if "%skipadmincheck%" == "1" echo.      Skipping administrator permissions check has been enabled in settings.
if "%skipversioncheck%" == "1" echo.      Skipping OS Version compatibility check has been enabled in settings.
if "%skipversioncheck%" == "1" echo.
echo.==================================================================================
choice /C YN /N /M "#################################[ 'Y'es/ 'N'o ]##################################"
if errorlevel 2 goto end
if errorlevel 1 goto checkosversion

:checkosversion
if "%debug%" == "1" echo. & pause>nul|set/p =[DEBUG] called checkosversion
if "%skipversioncheck%" == "1" goto successfulos
for /f "tokens=4-5 delims=. " %%i in ('ver') do set OSVERSION=%%i.%%j
if "%osversion%" == "10.0" set fancyosversion=Windows 10
if "%osversion%" == "6.3" set fancyosversion=Windows 8.1
if "%osversion%" == "6.2" set fancyosversion=Windows 8
if "%osversion%" == "6.1" set fancyosversion=Windows 7
if "%osversion%" == "6.0" set fancyosversion=Windows Vista
if "%osversion%" == "6.3" goto successfulos
if "%osversion%" == "6.2" goto successfulos
if "%osversion%" == "6.1" goto outdatedos
if "%osversion%" == "6.0" goto outdatedos
if "%osversion%" == "10.0" goto successfulos
goto :outdatedos

:outdatedos
cls
if "%debug%" == "1" echo. & pause>nul|set/p =[DEBUG] called outdatedos
echo.==============================================================================
echo.############################## OfficeFree65 ##################################
echo.==============================================================================
echo. 
echo. Your OS, %fancyosversion%, is not supported by this script.
echo.
echo. This script only supports Windows 10, 8.1 and 8
echo. 
echo. Advanced users: If you believe that the OS check has made a mistake,
echo. and your OS is among those that are supported, you can skip this check
echo. entirely by setting "skipversioncheck" to 1 in the settings section of this script.
echo. 
echo.==============================================================================
pause
goto end

:successfulos
goto :doesofficeexist

:doesofficeexist
if "%automaticdeleteoldofficeinstallations%" == "1" goto :deleteoldofficeinstallations
if "%automaticdeleteoldofficeinstallations%" == "2" goto :selectinstallationchannel
cls
if "%debug%" == "1" echo. & pause>nul|set/p =[DEBUG] asking if office already exists
if "%debug%" == "1" echo [DEBUG] OSVER %osversion%, FOSVER %fancyosversion%
echo.========================================================================================
echo.################################### OfficeFree65 #######################################
echo.========================================================================================
echo.
echo.  [1] Another version of Microsoft Office is already installed
echo.      Selecting this will uninstall all preexisting Office installations
echo.
echo.  [2] You do not have another version of Microsoft Office installed
echo.
echo.  [3] Another version of Microsoft Office is installed, but you do not want to delete it
echo.      Selecting this may cause issues upon installation of Microsoft Office 365
echo. 
echo.  If you are unsure, select [1]
echo.
echo.  Other options:
echo.
echo.  [4] Load utilities menu
echo.
echo.  [X] Go back
echo.
echo =======================================================================================
echo.
choice /C:1234X /N /M "	Enter Your Choice : "
if errorlevel 5 goto :promptifbegin
if errorlevel 4 goto :utilities
if errorlevel 3 goto :selectinstallationchannel
if errorlevel 2 goto :selectinstallationchannel
if errorlevel 1 goto :deleteoldofficeinstallations

:utilities
cls
set loadactivationfromutilities=0
set gotoutilitiesaftercompletion=0
if "%debug%" == "1" echo. & pause>nul|set/p =[DEBUG] loading utilities menu
echo.========================================================================================
echo.################################### OfficeFree65 #######################################
echo.========================================================================================
echo.
echo.  Utilities:
echo.
echo.  [1] Activate a version of Microsoft Office that is already installed
echo.
echo.  [2] Completely delete all currently installed versions of Microsoft Office
echo.
echo.  [3] Clean temproary files from previous script runs
echo.      Normaly this happens automatically, but prematurely ending the script prevents that.
echo.
echo.  Other options:
echo.
echo.  [X] Go back
echo.
echo ========================================================================================
echo.
choice /C:123X /N /M "	Enter Your Choice : "
if errorlevel 4 goto :doesofficeexist
if errorlevel 3 goto :cleanoldtempfiles
if errorlevel 2 goto :utilitydeleteoldofficeinstallations
if errorlevel 1 goto :loadactivationfromutilities

:cleanoldtempfiles
if "%debug%" == "1" echo. & pause>nul|set/p =[DEBUG] called cleanoldtempfiles
cls
echo.========================================================================================
echo.################################### OfficeFree65 #######################################
echo.========================================================================================
echo.
echo.  Cleaning old temproary files from previous script runs...
echo.
rmdir /q /s %temp%\deleteoldoffice >nul 2>&1
echo.  Deleted %temp%\deleteoldoffice, if it existed
rmdir /q /s %appdata%\officeinstallation >nul 2>&1
echo.  Deleted %appdata%\officeinstallation, if it existed
rmdir /q /s %appdata%\deleteoldoffice >nul 2>&1
echo.  Deleted %appdata%\deleteoldoffice, if it existed
rmdir /q /s %appdata%\3rikscript.bat >nul 2>&1
echo.  Deleted %appdata%\3rikscript.bat, if it existed
del %appdata%\C2R_Configold_20201021-1618.ini >nul 2>&1
echo.  Deleted %appdata%\C2R_Configold_20201021-1618.ini, if it existed
del %appdata%\C2R_Config_20201021-1618.ini >nul 2>&1
echo.  Deleted %appdata%\C2R_Config_20201021-1618.ini, if it existed
echo.
echo.  Successfuly cleaned most temproary files from previous script runs.
echo.  To clean the final folder, go back until you reach the EULA screen. Decline it to
echo.  exit out of the script properly. This will delete %temp%\OfficeFree65
echo.
echo.========================================================================================
echo.
echo.  Press any key to return to the utilities menu.
pause >nul
goto utilities

:loadactivationfromutilities
set loadactivationfromutilities=1
goto :activation

:utilitydeleteoldofficeinstallations
set gotoutilitiesaftercompletion=1
goto deleteoldofficeinstallations

:deleteoldofficeinstallations
cls
if "%debug%" == "1" pause>nul|set/p =[DEBUG]-DELETE OLD OFFICE BEGIN-Press any key to continue...
echo.========================================================================================
echo.################################### OfficeFree65 #######################################
echo.========================================================================================
echo.
echo.  Downloading Office purger script...
echo.
echo ========================================================================================
curl -L -o officepurger.bat "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/officepurger.bat"
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo ========================================================================================
echo.
echo.  Deleting previous Microsoft Office installations
echo.
echo.  This process may take some time
echo.
echo.  If the black window dosen't update for over 20 Mins, try pressing enter.
echo.
echo ========================================================================================
echo.
start /w officepurger.bat
if "%gotoutilitiesaftercompletion%" == "1" goto utilities
else goto selectinstallationchannel

:selectinstallationchannel
cd %temp%
cd OfficeFree65
mkdir officeinstallation
cd officeinstallation
if "%automaticchannel%" == "1" goto installmonthly
if "%automaticchannel%" == "2" goto installinsider
if "%automaticchannel%" == "3" goto installdogfooddevmain
cls
title %~f0 "OfficeFree65 - Select Installation Channel"
echo.========================================================================================
echo.################################### OfficeFree65 #######################################
echo.========================================================================================
echo.
echo.  You can select what channel you would like to install.
echo.  Available Channels:
echo.
echo.  [1] Current Monthly (Production:CC) - This is the standard channel.
echo.
echo.  [2] Beta Insider Fast (Insiders:DevMain) - This is the channel for beta builds.
echo.
echo.  [3] DevMain Channel (Dogfood::DevMain) - This is the channel for alpha builds.
echo.
echo.  If you are unsure, select [1]
echo.
echo.  Other options:
echo.
echo.  [X] Go back
echo.
echo =========================================================================================
echo.
choice /C:123X /N /M "	Enter Your Choice : "
if errorlevel 4 goto :doesofficeexist
if errorlevel 3 goto :installdogfooddevmain
if errorlevel 2 goto :installinsider
if errorlevel 1 goto :installmonthly

:: -----------------------------------------------------------------------------------
:: Monthly
:: -----------------------------------------------------------------------------------

:installmonthly
if "%skipantiviruswarning%" == "1" goto :installmonthlyafteravprompt
cls
echo.============================================================================================
echo.##################################### OfficeFree65 #########################################
echo.============================================================================================
echo.
echo.  It is reccomended to temproarily disable your antivirus KMS activation often triggers them.
echo.
echo.  This is because it makes an entry in your system services and the methods it uses to 
echo.  activate can be used in order to install malware onto your PC. This will trigger it as
echo.  "False Positive" in some antivirus programs.
echo.
echo.  You can also add an exclusion for "%temp%/OfficeFree65"
echo.  if you know how to do that.
echo.
echo.  In my testing, Windows Defender (or Windows Security) does not flag this as a virus,
echo.  so if that is your only antivirus software, you will likely not need to disable it.
echo.
echo.  If activation fails, you can disable your antivirus and rerun the activation part of
echo.  this script from the utilities menu available at the beginning.
echo.
echo.  Press any key to continue and begin installation...
echo. 
echo =============================================================================================
pause >nul
goto installmonthlyafteravprompt

:installmonthlyafteravprompt
title %~f0 "OfficeFree65 - Installing"
set channel=current
set fancychannel=Current Monthly (Standard)
cls
echo.========================================================================================
echo.################################### OfficeFree65 #######################################
echo.========================================================================================
echo.
echo.  Installing Microsoft Office 365 
echo.  Selected channel: %fancychannel%
echo.
echo =========================================================================================
echo.
echo.  Downloading required tools...
echo. 
echo =========================================================================================
curl -L -o YAOCTRU_Generator.cmd "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/YAOCTRU_Generatorcurrent.cmd"
call YAOCTRU_Generator.cmd
ren *_curl.bat curlofficemonthly.bat
set file=curlofficemonthly.bat
set newline=rem
set insertline=26
set output=curlofficemonthlyfixed.bat
(for /f "tokens=1* delims=[]" %%a in ('find /n /v "##" ^< "%file%"') do (
if "%%~a"=="%insertline%" (
echo %newline%
REM ECHO.%%b
) ELSE (
echo.%%b
)
)) > %output%
copy curlofficemonthly.batt curlofficemonthly.bat
call curlofficemonthly.bat
cls
echo.========================================================================================
echo.################################### OfficeFree65 #######################################
echo.========================================================================================
echo.
echo.  Installing Microsoft Office 365 
echo.  Selected channel: %fancychannel%
echo.
echo =========================================================================================
echo.
echo.  Downloading required tools...
echo. 
echo =========================================================================================
cd C2R_Monthly
curl -L -o C2R_Configold_20201021-1618.ini "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/C2R_Config.ini"
goto :officeversionupdater

:: -----------------------------------------------------------------------------------
:: Insider
:: -----------------------------------------------------------------------------------

:installinsider
if "%skipantiviruswarning%" == "1" goto :installinsiderafteravprompt
cls
echo.============================================================================================
echo.##################################### OfficeFree65 #########################################
echo.============================================================================================
echo.
echo.  It is reccomended to temproarily disable your antivirus KMS activation often triggers them.
echo.
echo.  This is because it makes an entry in your system services and the methods it uses to 
echo.  activate can be used in order to install malware onto your PC. This will trigger it as
echo.  "False Positive" in some antivirus programs.
echo.
echo.  You can also add an exclusion for "%temp%/OfficeFree65"
echo.  if you know how to do that.
echo.
echo.  In my testing, Windows Defender (or Windows Security) does not flag this as a virus,
echo.  so if that is your only antivirus software, you will likely not need to disable it.
echo.
echo.  If activation fails, you can disable your antivirus and rerun the activation part of
echo.  this script from the utilities menu available at the beginning.
echo.
echo.  Press any key to continue and begin installation...
echo. 
echo =============================================================================================
pause >nul
goto installinsiderafteravprompt

:installinsiderafteravprompt
title %~f0 "OfficeFree65 - Installing"
set channel=insider
set fancychannel=Insider (Beta)
cls
echo.========================================================================================
echo.################################### OfficeFree65 #######################################
echo.========================================================================================
echo.
echo.  Installing Microsoft Office 365 
echo.  Selected channel: %fancychannel%
echo.
echo =========================================================================================
echo.
echo.  Downloading required tools...
echo. 
echo =========================================================================================
curl -L -o YAOCTRU_Generator.cmd "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/YAOCTRU_Generatorinsider.cmd"
call YAOCTRU_Generator.cmd
ren *_curl.bat curlofficeinsider.bat
set file=curlofficeinsider.bat
set newline=rem
set insertline=26
set output=curlofficeinsiderfixed.bat
(for /f "tokens=1* delims=[]" %%a in ('find /n /v "##" ^< "%file%"') do (
if "%%~a"=="%insertline%" (
echo %newline%
REM ECHO.%%b
) ELSE (
echo.%%b
)
)) > %output%
copy curlofficeinsider.batt curlofficeinsider.bat
call curlofficeinsider.bat
cls
echo.========================================================================================
echo.################################### OfficeFree65 #######################################
echo.========================================================================================
echo.
echo.  Installing Microsoft Office 365 
echo.  Selected channel: %fancychannel%
echo.
echo =========================================================================================
echo.
echo.  Downloading required tools...
echo. 
echo =========================================================================================
cd C2R_InsiderFast
curl -L -o C2R_Configold_20201021-1618.ini "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/C2R_Configinsider.ini"
goto :officeversionupdater

:: -----------------------------------------------------------------------------------
:: DogfoodDevMain
:: -----------------------------------------------------------------------------------

:installdogfooddevmain
if "%skipantiviruswarning%" == "1" goto :installdevmainafteravprompt
cls
echo.============================================================================================
echo.##################################### OfficeFree65 #########################################
echo.============================================================================================
echo.
echo.  It is reccomended to temproarily disable your antivirus KMS activation often triggers them.
echo.
echo.  This is because it makes an entry in your system services and the methods it uses to 
echo.  activate can be used in order to install malware onto your PC. This will trigger it as
echo.  "False Positive" in some antivirus programs.
echo.
echo.  You can also add an exclusion for "%temp%\OfficeFree65"
echo.  if you know how to do that.
echo.
echo.  In my testing, Windows Defender (or Windows Security) does not flag this as a virus,
echo.  so if that is your only antivirus software, you will likely not need to disable it.
echo.
echo.  If activation fails, you can disable your antivirus and rerun the activation part of
echo.  this script from the utilities menu available at the beginning.
echo.
echo.  Press any key to continue and begin installation...
echo. 
echo =============================================================================================
pause >nul
goto installdevmainafteravprompt

:installdevmainafteravprompt
title %~f0 "OfficeFree65 - Installing"
set channel=devmain
set fancychannel=Dogfood DevMain (Alpha)
cls
echo.========================================================================================
echo.################################### OfficeFree65 #######################################
echo.========================================================================================
echo.
echo.  Installing Microsoft Office 365 
echo.  Selected channel: %fancychannel%
echo.
echo =========================================================================================
echo.
echo.  Downloading required tools...
echo. 
echo =========================================================================================
curl -L -o YAOCTRU_Generator.cmd "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/YAOCTRU_Generatordevmain.cmd"
call YAOCTRU_Generator.cmd
ren *_curl.bat curlofficedevmain.batt
set file=curlofficedevmain.bat
set newline=rem
set insertline=26
set output=curlofficedevmainfixed.bat
(for /f "tokens=1* delims=[]" %%a in ('find /n /v "##" ^< "%file%"') do (
if "%%~a"=="%insertline%" (
echo %newline%
REM ECHO.%%b
) ELSE (
echo.%%b
)
)) > %output%
copy curlofficedevmain.batt curlofficedevmain.bat
call curlofficedevmain.bat
cls
echo.========================================================================================
echo.################################### OfficeFree65 #######################################
echo.========================================================================================
echo.
echo.  Installing Microsoft Office 365 
echo.  Selected channel:  %fancychannel%
echo.
echo =========================================================================================
echo.
echo.  Downloading required tools...
echo. 
echo =========================================================================================
cd C2R_DogfoodDevMain
curl -L -o C2R_Configold_20201021-1618.ini "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/C2R_Configdevmain.ini"
goto :officeversionupdater

:officeversionupdater
if "%channel%" == "current" cd C2R_Monthly
if "%channel%" == "insider" cd C2R_InsiderFast
if "%channel%" == "devmain" cd C2R_DogfoodDevMain
mkdir OfficeFixes
cd OfficeFixes
mkdir win_x64
cd win_x64
curl -L -o wget.exe "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/wget.exe"
cd ..
cd ..
if "%debug%" == "1" dir
if "%debug%" == "1" echo %CD%
if "%debug%" == "1" pause>nul|set/p =[DEBUG]-GETTING OFFICERTOOL - Press any key to continue...
curl -L -o OfficeRTool.ini "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/OfficeRTool.ini"
if "%channel%" == "current" curl -L -o OfficeRToolcurrent.cmd "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/OfficeRToolcurrent.cmd"
if "%channel%" == "insider" curl -L -o OfficeRToolinsider.cmd "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/OfficeRToolinsider.cmd"
if "%channel%" == "devmain" curl -L -o OfficeRTooldevmain.cmd "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/OfficeRTooldevmode.cmd"
if "%debug%" == "1" pause>nul|set/p =[DEBUG]-RUNNING OFFICERTOOL - Press any key to continue...
if "%channel%" == "current" call OfficeRToolcurrent.cmd
if "%channel%" == "insider" call OfficeRToolinsider.cmd
if "%channel%" == "devmain" call OfficeRTooldevmain.cmd
if "%debug%" == "1" pause>nul|set/p =[DEBUG]-GOING TO PART 3 - Press any key to continue...
if "%channel%" == "current" goto installmonthlypart2
if "%channel%" == "insider" goto installinsiderpart2
if "%channel%" == "devmain" goto installdogfooddevmainpart2

:installmonthlypart2
del C2R_Configold_20201021-1618.ini
cd %temp%
cd OfficeFree65
cd officeinstallation
cd C2R_Monthly
curl -L -o YAOCTRI_Installer.cmd "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/YAOCTRI_Installer.cmd"
call YAOCTRI_Installer.cmd
mode con:cols=150 lines=40
color fc
cls
echo.========================================================================================
echo.################################### OfficeFree65 #######################################
echo.========================================================================================
echo.
echo.  Installing Microsoft Office 365 
echo.  Selected channel: %fancychannel%
echo.
echo =========================================================================================
echo.
echo.  Activating office...
echo. 
echo =========================================================================================
goto activation

:installinsiderpart2
del C2R_Configold_20201021-1618.ini
cd %temp%
cd OfficeFree65
cd officeinstallation
cd C2R_InsiderFast
curl -L -o YAOCTRI_Installer.cmd "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/YAOCTRI_Installer.cmd"
call YAOCTRI_Installer.cmd
mode con:cols=150 lines=40
color fc
cls
echo.========================================================================================
echo.################################### OfficeFree65 #######################################
echo.========================================================================================
echo.
echo.  Installing Microsoft Office 365 
echo.  Selected channel: %fancychannel%
echo.
echo =========================================================================================
echo.
echo.  Activating office...
echo. 
echo =========================================================================================
goto activation

:installdogfooddevmainpart2
del C2R_Configold_20201021-1618.ini
cd %temp%
cd OfficeFree65
cd officeinstallation
cd C2R_DogfoodDevMain
curl -L -o YAOCTRI_Installer.cmd "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/YAOCTRI_Installer.cmd"
call YAOCTRI_Installer.cmd
mode con:cols=150 lines=40
color fc
cls
echo.========================================================================================
echo.################################### OfficeFree65 #######################################
echo.========================================================================================
echo.
echo.  Installing Microsoft Office 365 
echo.  Selected channel:  %fancychannel%
echo.
echo =========================================================================================
echo.
echo.  Activating office...
echo. 
echo =========================================================================================
goto activation

:activation
title %~f0 "OfficeFree65 - Activating"
cls
echo.========================================================================================
echo.################################### OfficeFree65 #######################################
echo.========================================================================================
echo.
if "%loadactivationfromutilities%" == "1" echo.  Activating all installations of Microsoft Office
if "%loadactivationfromutilities%" == "1" echo.  Not guaranteed to work with Office versions not installed by this script
if "%loadactivationfromutilities%" == "0" echo.  Installing Microsoft Office 365
if "%loadactivationfromutilities%" == "0" echo.  Selected channel:  %fancychannel%
echo.
echo =========================================================================================
echo.
echo.  Downloading required tools...
echo. 
echo =========================================================================================
curl -L -o KMS_VL_ALL_AIO.cmd "https://codeberg.org/3rik/OfficeFree65/raw/branch/main/Recources/KMS_VL_ALL_AIO.cmd"
cls
echo.========================================================================================
echo.################################### OfficeFree65 #######################################
echo.========================================================================================
echo.
if "%loadactivationfromutilities%" == "1" echo.  Activating all installations of Microsoft Office
if "%loadactivationfromutilities%" == "1" echo.  Not guaranteed to work with Office versions not installed by this script
if "%loadactivationfromutilities%" == "0" echo.  Activating Microsoft Office 365
if "%loadactivationfromutilities%" == "0" echo.  Selected channel:  %fancychannel%
echo.
echo =========================================================================================
echo.
echo.  Activation script loading...
echo. 
echo =========================================================================================
call KMS_VL_ALL_AIO.cmd
mode con:cols=150 lines=40
color fc
if "%loadactivationfromutilities%" == "1" goto utilities
goto successfulinstallation

:successfulinstallation
cls
color fc
echo.================================================================================================
echo.####################################### OfficeFree65 ###########################################
echo.================================================================================================
echo.
echo ------------------------------------------------------------------------------------------------
echo   Office has been successfully installed and activated
echo   Channel installed: %fancychannel%
echo ------------------------------------------------------------------------------------------------
echo.
echo ------------------------------------------------------------------------------------------------
echo   You may need to close and reopen office a couple times in order for all the settings to sync
echo   and for all features to load properly.
echo   It is done once you get a popup asking you to accept the license agreement.
echo ------------------------------------------------------------------------------------------------
echo.
echo ------------------------------------------------------------------------------------------------
echo   Report issues to https://codeberg.org/3rik/OfficeFree65/issues/
echo ------------------------------------------------------------------------------------------------
echo.
echo ------------------------------------------------------------------------------------------------
echo.  If Office has not been successfuly activated, and you hadn't disabled your antivurus:
echo.   - Disable your Antivirus
echo.   - Run this script again
echo.   - After accepting the terms, select 4 to open the utilities menu
echo.   - Select 1 to try to activate Office 365 again
echo ------------------------------------------------------------------------------------------------
echo.
echo   Press any key to exit . . .
pause >nul
goto end

:end
if "%debug%" == "1" echo. & pause>nul|set/p =[DEBUG] called end
set calledfrommainscript=1
call %launcherscriptlocation%